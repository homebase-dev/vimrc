set nocompatible                           " be iMproved, required
filetype off                               " disable filetype detection

"set t_Co=256                               " fix for tmux (start with tmux -2)

set number                                 " show line numbers
set relativenumber                         " show line number only for current line (other relative)
set cursorline                             " highligth line in which cursor is
set mouse=
set diffopt+=vertical                      " Always use vertical diffs
set title                                  " show filename in title

" Search
set hlsearch                               " Highlight matches
set ignorecase                             " Case-insensitive search...
set smartcase                              " ...unless search contains uppercase letter
"set incsearch                              " Incremental search

" Indentation
set smarttab                               " better tabs
set tabstop=2                              " a tab is 2 spaces
set softtabstop=2                          " tab key -> 2 spaces, backspaces -> remove 2 spaces
set shiftwidth=2                           " indenting is 2 spaces
set autoindent                             " copy indentation from previous line
set smartindent                            " insert new level of indentation
set expandtab                              " always use spaces instead of tabs

"set magic                                  " For regular expressions turn magic on
set showmatch                              " Show matching brackets when text indicator is over them
set matchpairs+=<:>                        " Include angle brackets in matching

"set gdefault                               " Add the g flag to search/replace by default

" No beeps
set belloff=all

" Enchance :cmd xxx tab-completion
set wildmenu                               " Enhance command-line completion
set wildmode=longest:full,list:full        " Type of wildmenu
set wildignore=*.swp,*.swo                 " Ignore when expanding wildcards

set clipboard=unnamed                      " Use the OS clipboard by default (on versions compiled with `+clipboard`), enables cross-vim instance copy&paste
"set clipboard^=unnamedplus

set ttyfast                                " Optimize for fast terminal connections

" Backup & swap files
"set noswapfile                             " disable swapfiles
"set nobackup                               " disable backupfiles
"set undofile                               " maintain undo history between vim sessions
"set undolevels=100                         " Set maximum number of changes that can be undone

set backspace=indent,eol,start             " Allow backspacing over everything in insert mode
set splitright                             " when splitting window, open new window on the right (e.g. open file in new split window with ctrl-v in CtrlP fileselector)
"set termguicolors                          " enable true colors support, required by gruvbox, nord, etc. themes
"if exists('+termguicolors')               " tmux fix for true colors
"  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
"  set termguicolors
"endif
set hidden                                 " Hide buffers instead of closing them
set list                                   " show whitespace characters
set listchars=trail:·,nbsp:⎵,eol:¬,tab:▸\  " set custom whitespace chars

"match ErrorMsg '\s\+$'                    " highlight trailing spaces as error


" Remember info about open buffers on close
set viminfo^=%

let mapleader=" "                          " leader key is spacebar

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif


" Auto indent what you just pasted (TODO test if working)
"nnoremap >< V`]>
"nnoremap <lt>> V`]<
"nnoremap =- V`]=

" quickly go into search mode
map <Leader><Leader> :ag 
" open CtrlP fileselector
nnoremap <Leader>o :CtrlP<CR>
" open CtrlP bufferselector
nnoremap <Leader>b :CtrlPBuffer<CR>
nnoremap <Leader>w :w!<CR>
nnoremap <Leader>t :NERDTree<CR>
nnoremap <Leader>q :q!<CR>
" search for word under cursor (nail)
nnoremap <leader>n :exe 'Ack!' expand('<cword>')<cr>
" press // to search for visually selected text (not required anymore -> see above)
"vnoremap // y/<C-R>"<CR>
" substitute word under cursor
:nnoremap <Leader>s :%s/\<<C-r><C-w>\>/
" copy among vim instances (copy to + register)
:noremap <F5> "+yy<CR>
" paste from + register
:noremap <F6> "+p<CR>

" Buffers
nnoremap <leader>bd :bdelete<CR>
nnoremap <leader>bf :bfirst<CR>
nnoremap <leader>bl :blast<CR>
nnoremap <leader>bn :bnext<CR>
nnoremap <leader>bp :bprevious<CR>

" Quicker window movement
"nnoremap <C-j> <C-w>j
"nnoremap <C-k> <C-w>k
"nnoremap <C-h> <C-w>h
"nnoremap <C-l> <C-w>l

"########################################## Set filetypes for otherwise unknown files

au BufRead,BufNewFile *.hsp    set filetype=javascript  " use javascript syntaxhighlighting for *.hsp files
au BufRead,BufNewFile *.skin   set filetype=javascript  " use javascript syntaxhighlighting for *.skin files

"########################################## Vundle Plugin Manager Settings
"
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'scrooloose/nerdtree'              " Filetree window
Plugin 'ctrlpvim/ctrlp.vim'                " fuzzy finder
Plugin 'vim-airline/vim-airline'          " Add airline (statusbar) to vim
Plugin 'vim-airline/vim-airline-themes'   " Status line themes
Plugin 'tpope/vim-fugitive'               " git plugin for vim (:Gdiff, Gstatus, .., Glog -> :copen (see file-revisions))
Plugin 'tpope/vim-commentary'             " toggle code comments
Plugin 'tpope/vim-eunuch'                 " vim sugar for the unix shell commands that need it the most (:Delete, :Rename, :Chmod, :Mkdir, :SudoWrite, :SudoEdit, etc)
Plugin 'tpope/vim-jdaddy'                 " JSON helpers, pretty print JSON objects etc (aj, gqaj, gwaj, ij)
Plugin 'tpope/vim-characterize'           " ga on character reveals its representation in decimal, octal, hex, unicode, vim diagraphs, emoji code and html entities code
"Plugin 'vim-syntastic/syntastic'
" seems not to work
"Plugin 'ntpeters/vim-better-whitespace'  " show trailing whitespaces
"Plugin 'mxw/vim-jsx'                     " syntax highlighting and indentation for jsx
Plugin 'mechatroner/rainbow_csv'
Plugin 'mileszs/ack.vim'                  " recommanded replacement for ag.vim (requires config, see below)
Plugin 'shime/vim-livedown'               " render markdown out of vim
"Plugin 'neoclide/vim-jsx-improve'
"Plugin 'prettier/vim-prettier'           " auto formats code
"Plugin 'vim-scripts/vsutil.vim'
"Plugin 'vim-scripts/VimRegEx.vim'
Plugin 'pearofducks/ansible-vim'          " vim syntax plugin for Ansible 2.x (yaml, jinja2, hosts file)
Plugin 'mbbill/undotree'                   " add an undo tree (:UndotreeShow)
"Plugin 'airblade/vim-gitgutter'             " shows git diff (+|- lines by bg-color) in the 'gutter' (sign column)
Plugin 'easymotion/vim-easymotion'          " Quick-jump within screen by <leader><leader>s + enter char + enter selectedPosChar
"Plugin 'unblevable/quick-scope'             " An always-on highlight for a unique character in every word on a line to help you use f, F and family.
Plugin 'ycm-core/YouCompleteMe'           " Autocompletion for different programming languages

" React development
Plugin 'pangloss/vim-javascript'          " Javascript syntax highlighting
"Plugin 'othree/yajs.vim'                  " Yet Another JavaScript Syntax (better than above??)
Plugin 'mxw/vim-jsx'                      " JSX syntax highlighting
Plugin 'mattn/emmet-vim'                  " Write haml/slim and expand it to html
Plugin 'w0rp/ale'                         " Asynchronous Lint Engine (Syntax checker - requires ESLint)
"Plugin 'Shougo/deoplete.nvim'             " Autocomplete Feature (requires vim compiled with phyton3
"Plugin 'Valloric/YouCompleteMe'           " Another Autocomplete Feature (also requires vim build with phyton support)
"Plugin 'zxqfl/tabnine-vim'                " Another simple autocomplete feature (uses internally youcompleteme plugin)
Plugin 'prettier/vim-prettier'            " auto formats code

" LaTex
Plugin 'lervag/vimtex'                     " Tex support for vim

" Colorschemes
Plugin 'morhetz/gruvbox'                   " gruvbox colorschema
Plugin 'ayu-theme/ayu-vim'
Plugin 'arcticicestudio/nord-vim'
Plugin 'flazz/vim-colorschemes'
"
"Plugin 'jeffkreeftmeijer/vim-numbertoggle'
"Plugin 'powerline/powerline'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

function TrimWhitespace()
  %s/\s*$//
  ''
:endfunction

command! Trim call TrimWhitespace()
" remove blank lines
command Nobl :g/^\s*$/d
" fix syntax highlighting
command FixSyntax :syntax sync fromstart
" remove trailing white space
command Nows :%s/\s\+$//
" toggle paste mode + set mousemode depending on paste value
:noremap <F2> :set paste! nopaste? <bar> :exec &paste? "set mouse=" : "set mouse=a" <bar> :exec &paste? ":startinsert" : "" <CR>
"
"map <F3> <ESC>:exec &mouse!=""? "set mouse=" : "set mouse=nv"<CR>
":noremap <F3> :exec &paste? "set mouse=" : "set mouse=a"<CR>
command Sjs :set syntax=javascript         " define command :sjs (:set syntax=javascript)
" save file which you forgot to open with sudo
command SW :w !sudo tee % > /dev/null
cnoremap w!! w !sudo tee % >/dev/null

" Clear search highlight
nnoremap <leader>h :noh<CR>

" Undo across vim sessions
"if has("persistent_undo")
"  set undodir=~/.vim/undodir
"  set undofile
"endif

" shows the highlight group of whatever's under the cursor
" map <F3> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
" \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
" \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>



"########################################## Auto Commands

" Watch my .vimrc
"augroup reload_vimrc
"  autocmd!
"  autocmd BufWritePost $MYVIMRC source $MYVIMRC
"augroup end


"########################################## Settings for Plugins


" toggle comments (with vim-commentary plugin) -> use: gc + motion / gcap: whole paragraph or :g/TODO/Commentary
"map // gcc

" Ale configuration
let g:ale_sign_error = '💣'
let g:ale_sign_warning = '⚠'
"let g:ale_sign_error = "◉"
"let g:ale_sign_warning = "◉"
"let g:ale_sign_error = '✖' " Less aggressive than the default '>>' ●
"let g:ale_sign_warning = '.'
"let g:ale_lint_on_enter = 0 " Less distracting when opening a new file


" Emmet configuration

let g:user_emmet_leader_key='<Tab>'
let g:user_emmet_settings = {
  \  'javascript.jsx' : {
    \      'extends' : 'jsx',
    \  },
  \}

" vim-jsx configuration
let g:jsx_ext_required = 0

" ack.vim configuration (integrates silver searcher)
let g:ackprg = 'ag --vimgrep --smart-case'
cnoreabbrev ag Ack
cnoreabbrev aG Ack
cnoreabbrev Ag Ack
cnoreabbrev AG Ack



"" syntastic plugin recommended settings by website
"nnoremap <F7> :SyntasticCheck<CR> :lopen<CR>
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
""let g:syntastic_javascript_checkers = ['jshint']
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
""let g:syntastic_check_on_wq = 0
""let g:syntastic_javascript_eslint_exe='$(npm bin)/eslint'
"let g:syntastic_javascript_checkers = ['eslint']
""let g:syntastic_javascript_eslint_exe = '/usr/local/bin/eslint_proxy'
""let g:syntastic_javascript_eslint_exe = 'npm run lint --'


" vim-jsx plugin configuration
"let g:jsx_ext_required = 0


" vim-prettier plugin configuration
let g:prettier#config#bracket_spacing = 'true'
let g:prettier#config#semi = 'true'
let g:prettier#config#jsx_bracket_same_line = 'false'
let g:prettier#config#single_quote = 'true'
let g:prettier#config#trailing_comma = 'none'
" use F9 to trigger Prettier
nnoremap <F9> :Prettier<CR>

" ctrlp plugin configuration
"
" öffnet ctrlp im current dir
let g:ctrlp_working_path_mode = 0
"colorscheme torte



" easymotion plugin configuration
"
" Override <leader><leader> mapping since I already use it for ack (required for me!)
map <Leader> <Plug>(easymotion-prefix)
" Quickly jump through whole window (among splits)
nmap ö <Plug>(easymotion-overwin-f)
" Quickly jump to through current split
map <leader>ö <Plug>(easymotion-bd-f)


"let g:ctrlp_prompt_mappings = {
"    \ 'AcceptSelection("e")': ['<c-t>'],
"    \ 'AcceptSelection("t")': ['<cr>', '<2-LeftMouse>'],
"    \ }


"########################################## Colorscheme Settings

"colorscheme torte                     " a good scheme if no theme plugin installed

" use ayu colorscheme: https://github.com/ayu-theme/ayu-vim
"let ayucolor="mirage"
"colorscheme ayu

" use nord-vom colorscheme: https://github.com/arcticicestudio/nord-vim
"let g:nord_italic = 1
"let g:nord_underline = 1
"let g:nord_italic_comments = 1
"let g:nord_uniform_status_lines = 1
"let g:nord_comment_brightness = 12
"let g:nord_uniform_diff_background = 1
"let g:nord_cursor_line_number_background = 1
"colorscheme nord

" use gruvbox colorscheme
set background=dark                    " Setting dark mode
let g:gruvbox_bold = 1
let g:gruvbox_italic = 1
let g:gruvbox_underline = 1
let g:gruvbox_undercurl = 1
let g:gruvbox_contrast_dark = 'soft'
"let g:gruvbox_hls_cursor = orange
"let g:gruvbox_number_column = none
"let g:gruvbox_italicize_comments = 1

colorscheme gruvbox

let g:airline_theme='gruvbox'
let g:airline_powerline_fonts=1

set modeline                            " Look for embedded modelines at the top of the file
syntax on
