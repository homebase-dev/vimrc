# vimrc

My .vimrc file. Please note that it requires a plugin manager (e.g. [Vundle](https://github.com/VundleVim/Vundle.vim)) for installing all the plugins.

